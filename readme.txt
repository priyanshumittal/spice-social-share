=== Spice Social Share ===

Contributors: 		spicethemes
Tags: 			social, facebook share, twitter share, linkedin share, whatsapp
Requires at least: 	5.3
Requires PHP: 		5.2
Tested up to: 		6.7.1
Stable tag: 		1.2.2
License: 		GPLv2 or later
License URI: 		https://www.gnu.org/licenses/gpl-2.0.html

Effortlessly add social share buttons to your posts.

== Description ==

This plugin allows you to add social share buttons to your posts. The plugin is flexible and easy to use.

<h3>Key Features</h3>
* Supports Facebook, Twitter, LinkedIn, Email, Pinterest, WhatsApp, Print share buttons.
* Typography setting
* Color setting
* To find the Typography And Color settings Go to Appearance >> Customize >>Spice Social Share>> Find the respective settings.

== Changelog ==

@Version 1.2.2
* Fixed PCP plugin related issues.

@Version 1.2.1
* Fixed Warning deprecated dynamic property creation by adding public variables plugin_url and plugin_path inside the class Spice_Social_Share

@Version 1.2
* Updated Font Awesome library and twitter icon.
* Added WhatsApp and Print icons.

@Version 1.1
* Added name attribute value for social icon button. 

@Version 1.0
* Update Tested up to field value.
* Added Description about How to find Typography and Color settings 

@Version 0.1
* Initial release

======= External Resources =======

Font Awesome
Copyright: (c) Dave Gandy
License: https://fontawesome.com/license ( Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License )
Source: https://fontawesome.com

Customizer Sortable
Kirki, Copyright © 2016 Aristeides Stathopoulos
License: MIT License, http://www.opensource.org/licenses/MIT
Source: https://github.com/aristath/kirki

Customizer Toggle Control
Copyright: (c) 2016 soderlind
License: Under GNU General Public License v2.0
Source: https://github.com/soderlind/class-customizer-toggle-control